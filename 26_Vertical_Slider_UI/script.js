const silderContainer = document.querySelector(".slider-container");
const sildeRight = document.querySelector(".right-slide");
const sildeLeft = document.querySelector(".left-slide");
const upButton = document.querySelector(".up-button");
const downButton = document.querySelector(".down-button");
const slidesLenght = sildeRight.querySelectorAll("div").length;
console.log(slidesLenght);

let activeSlideIndex = 0;
sildeLeft.style.top = `-${(slidesLenght - 1) * 100}vh`;

upButton.addEventListener("click", () => changeSlide("up"));
downButton.addEventListener("click", () => changeSlide("down"));

const changeSlide = (direction) => {
  const sliderHight = silderContainer.clientHeight;
  if (direction == "up") {
    activeSlideIndex++;

    if (activeSlideIndex > slidesLenght - 1) {
      activeSlideIndex = 0;
    }
  } else if (direction == "down") {
    activeSlideIndex--;
    if (activeSlideIndex < 0) {
      activeSlideIndex = slidesLenght - 1;
    }
  }

  sildeRight.style.transform = `translateY(-${
    activeSlideIndex * sliderHight
  }px)`;

  sildeLeft.style.transform = `translateY(${activeSlideIndex * sliderHight}px)`;
};

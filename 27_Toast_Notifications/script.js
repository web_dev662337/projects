const button = document.getElementById("button");
const toasts = document.getElementById("toasts");

const meesages = [
  "Message one",
  "Message two",
  "Message three",
  "Message four",
  "Message five",
];

const types = ["info", "success", "error"];

button.addEventListener("click", () => createNotyfication());

function createNotyfication(meesage = null, type = null) {
  const notif = document.createElement("div");
  notif.classList.add("toast");
  notif.classList.add(type ? type : getRandomType());
  notif.innerText = meesage ? meesage : getRandomMessage();

  toasts.appendChild(notif);

  setTimeout(() => {
    notif.remove();
  }, 3000);
}

function getRandomMessage() {
  return meesages[Math.floor(Math.random() * meesages.length)];
}

function getRandomType() {
  return types[Math.floor(Math.random() * types.length)];
}

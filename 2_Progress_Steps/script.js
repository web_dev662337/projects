const progress = document.getElementById("progress");
const prev = document.getElementById("prev");
const next = document.getElementById("next");
const circles = document.querySelectorAll(".circle");

let currnetActive = 1;

next.addEventListener("click", () => {
  console.log(currnetActive);
  currnetActive++;
  if (currnetActive > circles.length) {
    currnetActive = circles.length;
  }

  update();
});

prev.addEventListener("click", () => {
  currnetActive--;
  if (currnetActive < 1) {
    currnetActive = 1;
  }

  update();
});

function update() {
  circles.forEach((circle, idx) => {
    circle.classList.add("active");
    if (idx < currnetActive) {
      circle.classList.add("active");
    } else {
      circle.classList.remove("active");
    }
  });

  const actives = document.querySelectorAll(".active");
  progress.style.width =
    ((actives.length - 1) / (circles.length - 1)) * 100 + "%";

  if (currnetActive === 1) {
    prev.disabled = true;
  } else if (currnetActive === circles.length) {
    next.disabled = true;
  } else {
    prev.disabled = false;
    next.disabled = false;
  }
}

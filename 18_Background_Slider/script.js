const body = document.body;
const slieds = document.querySelectorAll(".slide");
const leftBtn = document.getElementById("left");
const rightBtn = document.getElementById("right");

let actieSlide = 0;

setBgToBody();

rightBtn.addEventListener("click", () => {
  actieSlide++;
  if (actieSlide > slieds.length - 1) {
    actieSlide = 0;
  }
  setActiveSlide();
  setBgToBody();
});

leftBtn.addEventListener("click", () => {
  actieSlide--;
  if (actieSlide < 0) {
    actieSlide = slieds.length - 1;
  }
  setActiveSlide();
  setBgToBody();
});

function setBgToBody() {
  body.style.backgroundImage = slieds[actieSlide].style.backgroundImage;
}

function setActiveSlide() {
  slieds.forEach((slide) => {
    slide.classList.remove("active");
  });

  slieds[actieSlide].classList.add("active");
}
